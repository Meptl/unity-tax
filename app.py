# app.py

from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request
import plotly.express as px

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')


def get_tax(brackets, installs):
    tax_amount = 0
    prev_threshold = 0
    remaining_price = brackets[0][0]
    for threshold, price in brackets:
        if installs <= threshold:
            remaining_price = price
            break
        tax_amount += (threshold - prev_threshold) * price
        prev_threshold = threshold
    tax_amount += (installs - prev_threshold) * remaining_price
    return tax_amount


def unity_personal(value):
    return value * 0.2


def unity_pro(installs):
    tax_brackets = [
        (100000, 0.15),
        (500000, 0.075),
        (1000000, 0.03),
        (float('inf'), 0.02),
    ]
    return get_tax(tax_brackets, installs)


def unity_enterprise(installs):
    tax_brackets = [
        (100000, 0.125),
        (500000, 0.06),
        (1000000, 0.02),
        (float('inf'), 0.01),
    ]
    return get_tax(tax_brackets, installs)


@app.route('/data')
def data():
    personal_installs = list(range(0, 500000, 10000))
    installs = list(range(0, 2000000, 10000))
    return jsonify(
        {
            "personal": {
                'installs': personal_installs,
                'tax': [unity_personal(val) for val in personal_installs]
            },
            "pro": {
                'installs': installs,
                'tax': [unity_pro(val) for val in installs]
            },
            "enterprise": {
                'installs': installs,
                'tax': [unity_enterprise(val) for val in installs]
            }
        }
    )

@app.route('/calculate', methods=['POST'])
def calculate():
    option = request.form.get('option')
    value = int(request.form.get('value'))
    tax = 0
    if option == 'personal':
        tax = unity_personal(value)
    elif option == 'pro':
        tax = unity_pro(value)
    else:
        tax = unity_enterprise(value)
    return jsonify({'result': tax})

if __name__ == '__main__':
    app.run(debug=False)
