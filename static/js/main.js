fetch('/data')
.then(response => response.json())
.then(data => {
  var trace1 = {
    x: data.pro.installs,
    y: data.pro.tax,
    mode: 'lines',
    type: 'scatter',
    name: 'Unity Pro'
  };

  var trace2 = {
    x: data.enterprise.installs,
    y: data.enterprise.tax,
    mode: 'lines',
    type: 'scatter',
    name: 'Unity Enterprise'
  };

  var trace3 = {
    x: data.personal.installs,
    y: data.personal.tax,
    mode: 'lines',
    type: 'scatter',
    name: 'Unity Personal'
  };

  var layout = {
    title:'Install Tax Calculator',
    hovermode: 'closest',
    xaxis: {
      title: 'Install',
    },
    yaxis: {
      title: 'Tax',
    }
  };

  Plotly.newPlot('myDiv', [trace1, trace2, trace3], layout);
});
